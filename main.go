package distore

import (
	"encoding/binary"
	"fmt"
	"reflect"
	"runtime/debug"
	"syscall"
	"unsafe"

	"gitea.com/abgr/aerr"
)

type ID uintptr

func init() {
	debug.SetPanicOnFault(true)
}
func mmap(length int, prot int, flags int) (xaddr uintptr, err error) {
	xaddr, _, e1 := syscall.Syscall6(syscall.SYS_MMAP, 0, uintptr(length), uintptr(prot), uintptr(flags), 0, 0)
	if e1 != 0 {
		fmt.Println(uintptr(e1))
		err = e1
	}
	return
}
func Store(buff []byte, share bool) ID {

	aerr.Assert(len(buff) > 0, aerr.New("distore: empty buffer"))
	aerr.Assert(uint64(len(buff)) < uint64(^uint32(0)), aerr.New("distore: too large buffer"))
	flags := syscall.MAP_ANONYMOUS
	if share {
		flags |= syscall.MAP_SHARED
	} else {
		flags |= syscall.MAP_PRIVATE
	}
	ptr, err := mmap(
		4+len(buff),
		syscall.PROT_READ|syscall.PROT_WRITE,
		flags,
	)
	aerr.Panicerr(err, nil)
	binary.BigEndian.PutUint32((*(*[4]byte)(unsafe.Pointer(ptr)))[:], uint32(len(buff)))
	for i := uintptr(0); i < uintptr(len(buff)); i++ {
		*(*byte)(unsafe.Pointer(ptr + 4 + i)) = buff[i]
	}
	return ID(ptr)
}

func cap(di uintptr) uint32 {
	b := *(*[4]byte)(unsafe.Pointer(di))
	return binary.BigEndian.Uint32(b[:])
}
func SizeOf(di ID) int {
	defer aerr.Ignore()
	return int(cap(uintptr(di)))
}
func load(di uintptr) (b []byte) {
	c := int(cap(di))
	b = *(*[]byte)(unsafe.Pointer(&reflect.SliceHeader{
		Data: di + 4,
		Len:  c,
		Cap:  c,
	}))
	return
}

func munmap(addr uintptr, length uint32) {
	syscall.Syscall(syscall.SYS_MUNMAP, addr, uintptr(length), 0)
}

func Purge(di ID) {
	defer aerr.Ignore()
	munmap(uintptr(di), cap(uintptr(di))+4)
}
func Load(di ID, clone bool) (b []byte, err error) {
	defer func() {
		err, _ = recover().(error)
	}()
	if clone {
		b2 := load(uintptr(di))
		b = make([]byte, len(b2))
		copy(b, b2)
	} else {
		b = load(uintptr(di))
	}
	return
}
